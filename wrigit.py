#!/usr/bin/env python
# -*- mode: python -*-

import os, time, datetime
from os.path   import join, abspath

import rst
from cfg  import o, setup_cfg
cfg = setup_cfg([
        o('-c', '--conf', dest='conf',
          help='Configuration file'),
        o('-r', '--rstdir', dest='rstdir',
          help='Directory where source .rst files are found'),
        o('-t', '--templatedir', dest='templatedir',
          help='Directory where mako templates can be found'),
        o('-b', '--baseurl', dest='baseurl',
          help='The base URL of the site'),
        o('-T', '--title', dest='title',
          help='Site title that goes into <title>'),
        o('', '--regen', dest='regen', action="store_true",
          help='Regenerate the whole site (all files)'),
        o('-d', '--disqus-forum', dest='disqus_forum',
          help='The name of the disqus forum. For example, ' +
          'if http://foo.disqus.com is the forum, then do --disqus-forum=foo'),
        o('', '--blog-title', dest='blog.title',
          help='Blog title'),
        o('', '--blog-subtitle', dest='blog.subtitle',
          help='Blog title'),
        o('', '--feedburner-url', dest='blog.feedburner_url',
          help='FeedBurner URL to use instead of local atom.xml'),
        o('', '--google-analytics', dest='google_analytics',
          help='Your google analytics code (eg: UA-1563463-1)'),
        ], {'regen': False})

cfg['rstdir'] = abspath(cfg['rstdir'])
cfg['templatedir'] = abspath(cfg['templatedir'])

from util import make_mako, last_modified, rfc3339


class Page(object):
    """A rst page"""
    
    def __init__(self, rstfile):
        parts, meta = rst.process(rstfile)
        
        self.rstfile   = rstfile
        self.title     = parts['title']
        self.fragment  = parts['fragment']
        self.meta      = meta

    def title_html(self):
        return '%s - %s' % (self.title, cfg['title'])

    def date(self):
        if '|' in self.meta['date']:
            format = '%Y-%m-%d|%H:%M'
        else:
            format = '%Y-%m-%d'
            
        return datetime.datetime(
            *time.strptime(self.meta['date'],
                           format)[0:6])

    def updated_date(self):
        return datetime.datetime.fromtimestamp(
            last_modified(self.rstfile))

    def tags(self):
        tags = self.meta.get('tags', '').split()
        tags.sort()
        return tags

    def url(self):
        path_info = rst.htmlpath(self.rstfile[len(cfg['rstdir']):])
        return cfg['baseurl'] + path_info

    def commentable(self):
        return False

class BlogPost(Page):

    def title_html(self):
        return self.title

    def commentable(self):
        return True

    def __cmp__(self, other):
        return cmp(self.date(), other.date())

    def __repr__(self):
        return '<BlogPost "%s" on %s>' % (self.title, self.date())


if __name__ == '__main__':
    # Import Psyco if available
    try:
        import psyco
        psyco.full()
    except ImportError:
        pass

    print '[%s - %s] rstdir:%s [using templatedir:%s]' % (
        cfg['baseurl'], cfg['title'], cfg['rstdir'], cfg['templatedir'])

    rstdir = cfg['rstdir']
    blog_posts = []
    blog_path = join(rstdir, 'blog')
    feed_url = cfg['blog.feedburner_url'] or cfg['baseurl'] + '/blom/atom.xml'
    
    for f in rst.rstfiles(rstdir):
        if blog_path in f:
            page = BlogPost(f)
            blog_posts.append(page)
        else:
            page = Page(f)
            
        make_mako(rst.htmlpath(f),
                  [f],
                  'page.mako')

    blog_posts.sort()
    blog_posts.reverse()

    # generate blog index
    make_mako(join(rstdir, 'blog', 'index.html'),
              [p.rstfile for p in blog_posts],
              'blog/index.mako')

    # generate blog feed
    recent_blog_posts = blog_posts
    updated = max([p.updated_date()
                   for p in recent_blog_posts])
    recent_blog_posts = recent_blog_posts[:10]
    make_mako(join(rstdir, 'blog', 'atom.xml'),
              [p.rstfile for p in recent_blog_posts],
              'blog/atom.xml.mako')


