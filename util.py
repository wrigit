import os, sys
from mako.lookup   import TemplateLookup

from cfg import cfg


rfc3339 = "%Y-%m-%dT%H:%M:%S"


def mako():
    templates = TemplateLookup(directories=[cfg['templatedir']])
    def render(name, **namespace):
        import cgi
        return templates.get_template(name).render(
            cgi=cgi,
            **namespace)
    return render
mako = mako()

# gnu make like functinality
def last_modified(filename):
    return os.stat(filename).st_mtime

def make(target, deps, content_generator, *args, **kwargs):
    if cfg['regen'] or not os.path.exists(target) or \
            True in [last_modified(target) < last_modified(d) for d in deps]:
        # requires update
        print '>', target
        content = content_generator(*args, **kwargs)
        open(target, 'w').write(content)

def make_mako(target, deps, name, **additional_namespace):
    """
    Wrapper around make/mako.  Introduces mako files itself as one of deps
    """
    templatefile = os.path.join(cfg['templatedir'], name)
    
    namespace = sys._getframe(1).f_locals
    namespace.update(additional_namespace)

    return make(target, deps+[templatefile], mako, name, **namespace)

__all__ = ['rfc3339', 'mako', 'make', 'make_mako', 'last_modified']
