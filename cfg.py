"""A simple wrapper around optparse and configobj

>>> from cfg import o, setup_cfg
>>> cfg = setup_cfg([
            o('-c', '--conf', dest='conf',
              help='Configuration file'),
              ...
              ], {'foo':  34})
>>> print cfg['bar.yaht']
...
"""

from os.path import dirname, abspath

import configobj
from optparse  import make_option as o, OptionParser

# Global 'cfg' object
cfg = None

# ConfigObj does not support passing custom template variables like in
# most HTML templating systems. This is bad. 
# ConfigObj code is also not very extensible. This is very bad.
# So we hack around the `interpolation_engines` global variable to
# get our job done.

def configobj_namespace(namespace):
    global configobj
    import re
    
    class GoodConfigParserInterpolation(configobj.ConfigParserInterpolation):
        """Resolve custom template variables manually passed"""

        def interpolate(self, key, value):
            try:
                return super(GoodConfigParserInterpolation,
                             self).interpolate(key, value)
            except configobj.InterpolationError:
                # eg: extract 'var' out of '%(var)s'
                match = self._KEYCRE.search(value)
                if match:
                    var = match.group(1)
                    if var in namespace:
                        return re.sub(self._KEYCRE, namespace[var], value)
                raise

    # There is another engine 'template' which, for me, never seems to
    # work. voidspace?
    configobj.interpolation_engines['configparser'] = \
        GoodConfigParserInterpolation


def married(options, configobj):
    """
    Marry optparse and configobj
    http://wiki.python.org/moin/ConfigParserShootout
    """
    
    class sex(object):

        def __setitem__(self, item, value):
            return setattr(options, item, value)

        def __getitem__(self, item):
            # If `item` is not found in `options` read from `configobj`
            try:
                value = getattr(options, item)
                if value is None:
                    raise AttributeError
                else:
                    return value
            except AttributeError:
                c = configobj
                try:
                    for a in item.split('.'):
                        c = c[a]
                except KeyError:
                    return None # optparse, too, returns None in this case.
                return c

        def __str__(self):
            return "<cfg {\n%s,\n\n%s\n}>" % (options, configobj)

    return sex()

def setup_cfg(option_list, defaults, namespace={}):
    global cfg
    
    oparser = OptionParser(option_list=option_list)
    oparser.set_defaults(**defaults)
    (options, args) = oparser.parse_args()

    if options.conf is None:
        raise SystemExit, "You must specify the --conf option."

    # `pwd` contains the directory where the conf file lies.
    namespace['pwd'] = abspath(dirname(abspath(options.conf)))
    
    # patch it!
    configobj_namespace(namespace)
        
    cobj = configobj.ConfigObj(options.conf)
    cfg = married(options, cobj)
    return cfg

__all__ = ['setup_cfg', 'o', 'cfg']
