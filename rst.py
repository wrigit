import os
from os.path import join, abspath

from docutils                   import core, nodes
from docutils.parsers           import rst
from docutils.writers.html4css1 import Writer, HTMLTranslator

from pygments            import highlight
from pygments.lexers     import get_lexer_by_name, TextLexer
from pygments.formatters import HtmlFormatter


# This is a temporary hack - to extract the 'custom fields' from
# docutil's `docinfo` node.
# 
# `HTMLTranslator` happens to process the docinfo tree and so we can steal
# the key,value pairs of custom fields from the `visit_field_name` function.
class DocInfoProxy(HTMLTranslator):

    ITEMS = {}

    def __init__(self, *args, **kwargs):
        HTMLTranslator.__init__(self, *args, **kwargs)
        DocInfoProxy.ITEMS = {}
        self.__items = {}

    # Custom fields are processed here (Tags and so on)
    def visit_field_name(self, node):
        key = node.astext().lower()
        value = node.parent.children[1].astext() # from sibling
        # print 'meta[%s] = {%s}' % (key, value)
        DocInfoProxy.ITEMS[key] = value
        return HTMLTranslator.visit_field_name(self, node)

    # Standard fields are processed here (Date, Author and so on)
    def visit_docinfo_item(self, node, name, meta=1):
        key = name.lower()
        value = node.parent.children[0].astext() # from sibling
        # print '*meta[%s] = {%s}' % (key, value)
        DocInfoProxy.ITEMS[key] = value
        return HTMLTranslator.visit_docinfo_item(self, node, name, meta)


def process(rstfile):
    "Process the `rstfile` and return the `parts` and `meta` information"
    writer = Writer()
    writer.translator_class = DocInfoProxy # see comment for `DocInfoProxy`
    
    parts = core.publish_parts(
        source=open(rstfile).read(),
        source_path=rstfile, writer=writer)
    meta = DocInfoProxy.ITEMS.copy()
    return parts, meta

def htmlpath(rstfile):
    """Return the .html path for the given .rst path"""
    assert rstfile.endswith('.rst'), '<%s> is not a rst path' % rstfile
    return rstfile[:-4] + '.html'

def rstfiles(directory):
    for dir, subdirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.rst'):
                yield abspath(join(dir, file))



# Pygments syntax-highlighting for ReST
# adapted from,
#  http://dev.pocoo.org/projects/pygments/browser/external/rst-directive.py

INLINESTYLES = False
DEFAULT = HtmlFormatter(noclasses=INLINESTYLES)
# Add name -> formatter pairs for every variant you want to use
VARIANTS = {
    # 'linenos': HtmlFormatter(noclasses=INLINESTYLES, linenos=True),
}

def pygments_directive(name, arguments, options, content, lineno,
                       content_offset, block_text, state, state_machine):
    try:
        lexer = get_lexer_by_name(arguments[0])
    except ValueError:
        # no lexer found - use the text one instead of an exception
        lexer = TextLexer()
    # take an arbitrary option if more than one is given
    formatter = options and VARIANTS[options.keys()[0]] or DEFAULT
    parsed = highlight(u'\n'.join(content), lexer, formatter)
    return [nodes.raw('', parsed, format='html')]

pygments_directive.arguments = (1, 0, 1)
pygments_directive.content = 1
pygments_directive.options = dict([(key, directives.flag) for key in VARIANTS])

rst.directives.register_directive('sourcecode', pygments_directive)


