<!-- -*- mode: html -*- -->

<!-- Functions -->

<%def name="render_page(page, sub=False)">
%if sub:
<h2>${page.title}</h2>
%else:
<h1 class="title">${page.title}</h1>
%endif
<div style="padding: 7px;">
  ${page.fragment}
</div>
<div style="float: right;">
  Posted on <span style="font-style: italic;">
	${page.date().strftime("%b %d, %y at %I:%M %p")}</span>
  %if page.tags():
  to
  <span style="color: blue;">
	%for tag in page.tags():
	${tag}
	%endfor
  </span>
  %endif
</div>
<div>
  <a href="${page.url()}">Link</a>
  %if page.commentable():
  |
  <a href="${page.url()}#disqus_thread">Comments</a>
  %endif
</div>

%if page.commentable() and sub is False:
<!-- disqus comments -->
<div id="disqus_thread"></div><script type="text/javascript" src="http://disqus.com/forums/${cfg['disqus_forum']}/embed.js"></script><noscript><a href="http://${cfg['disqus_forum']}.disqus.com/?url=ref">View the forum thread.</a></noscript>
%endif
<div style="clear: both;"></div>
</%def>
