<!-- -*- mode: html -*- -->
<%inherit file="/base.mako" />
<%namespace name="f" file="/functions.mako" />

<%def name="title()">${cfg['blog.title']}</%def>

<%def name="content()">
<h1 class="title">${self.title()}</h1>
% for page in blog_posts:
${f.render_page(page, sub=True)}
% endfor

<!-- disqus number of comments code -->
<script type="text/javascript">
//<[CDATA[
(function() {
  links = document.getElementsByTagName('a');
  query = '?';
  for(var i = 0; i < links.length; i++) {
    if(links[i].href.indexOf('#disqus_thread') >= 0) {
	  query += 'url' + i + '=' + encodeURIComponent(links[i].href) + '&';
    }
  }
  document.write('<script type="text/javascript" src="http://disqus.com/forums/${cfg['disqus_forum']}/get_num_replies.js' + query + '"></' + 'script>');
})()
;
//]]>
</script>

</%def>
