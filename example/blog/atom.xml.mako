## -*- coding: utf-8 -*-
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title>${cfg['blog.title']}</title>
  <subtitle>${cfg['blog.subtitle']}</subtitle>
  <link href="${cfg['baseurl']}/blog/" />
  <updated>${updated.strftime(rfc3339)}</updated>

  <author>
	<name>${cfg['author']}</name>
	<email>${cfg['author_email']}</email>
  </author>
  <id>${cfg['baseurl']}/blog/</id>

  <generator uri="http://repo.or.cz/w/wrigit.git">
	Wrigit
  </generator>

  %for p in recent_blog_posts:
  <entry>
	<title>${p.title}</title>
	<link href="${p.url()}" />
	<id>${p.url()}</id>
	<updated>${p.updated_date().strftime(rfc3339)}</updated> 
	<published>${p.date().strftime(rfc3339)}</published>
	<content type="html">${cgi.escape(p.fragment)}</content>
	
	%for t in p.tags():
	<category term="${t}" />
	%endfor
  </entry>
  %endfor

</feed>
