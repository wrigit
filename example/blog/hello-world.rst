Hello World
===========

:Date: 2007-12-16|15:45
:Tags: world python hello 

So _this_ is be my *first* blog post.

+------------------------+------------+----------+
| Header row, column 1   | Header 2   | Header 3 |
+========================+============+==========+
| body row 1, column 1   | column 2   | column 3 |
+------------------------+------------+----------+
| body row 2             | Cells may span        |
+------------------------+-----------------------+

.. sourcecode:: python

  def foo(x, y=[4, 5]):
      return x + "bar"


Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla sodales pede
vitae urna. Morbi vitae erat. Cras vitae elit vitae tortor iaculis gravida. Ut
et odio. Maecenas nibh elit, iaculis eget, hendrerit sit amet, rutrum a,
leo. Nam vitae odio vel leo congue tempus. Integer iaculis, odio a elementum
scelerisque, arcu est dignissim ipsum, et dictum diam velit quis ipsum. Integer
quis risus. Suspendisse tempor tellus non nulla. Nullam eget ipsum. Aliquam
vestibulum odio mollis turpis pharetra semper. Aliquam auctor.

.. note:: This is a paragraph

   - Here is a bullet list.

Fin.