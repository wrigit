<!-- -*- mode: html -*- -->
<%inherit file="base.mako" />
<%namespace name="f" file="functions.mako" />

<%def name="title()">${page.title_html()}</%def>

<%def name="content()">
${f.render_page(page)}
</%def>
