<!-- -*- mode: html -*- -->
<html>
  <head>
	<title>${self.title()}</title>
	<link rel="stylesheet" type="text/css" href="/style.css" />
	<link rel="stylesheet" type="text/css" href="/pygments-style.css" />
	<link rel="alternate" type="application/atom+xml"
		  title="${cfg['blog.title']} - Atom"
		  href="${feed_url}" />
  </head>
  
  <body>
	<div class="document">
	  ${self.content()}
	</div>
	
	<div class="footer">
	  <div style="float: left;">
		<a href="${feed_url}">feed</a>
	  </div>
	  <a href="/">home</a> |
	  <a href="/blog/">blog</a> |
	  <a href="http://repo.or.cz/w/wrigit.git">powered by wrigit</a>
	</div>

	%if cfg['google_analytics']:
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("${cfg['google_analytics']}");
pageTracker._initData();
pageTracker._trackPageview();
</script>
	%endif

  </body>
</html>
